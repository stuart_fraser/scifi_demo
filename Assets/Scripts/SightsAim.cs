﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace UnityStandardAssets.Characters.FirstPerson
{
    public class SightsAim : MonoBehaviour
    {
        //Head Bob variables
        public bool HeadBobEnabled = true;
        public Vector2 HeadBobDistance = new Vector2(0.03f, 0.1f);
        public float HeadBobSpeed = 4.5f;
        public Transform gunPos;
        public Transform playerCamera;
        public float duration = 0.3f;
        public float endDistance = 0.1f;
        public bool rightHanded = true;
        public float shakeOffset = 10.0f;
        public float shakeDur = 0.5f;
        public float shakeFreq = 1.0f;
        float shakeTimer = 0.0f;
        //Using Bezier to smooth animation.
        public AnimationCurve Curve = new AnimationCurve(new Keyframe(0.0f, 0.0f), new Keyframe(1.0f, 1.0f));
        //Get headbob script to change the headbob effect when aiming.
        bool empty = true;
        float ironsighttimer;
        float hiptimer;
        Vector3 start;
        Vector3 end;
        Vector3 localPos;
        Vector3 currentPos;
        float velocity;

        bool shakeActive = false;

        // Use this for initialization
        void Start()
        {
            if (gunPos != null && playerCamera !=null)
            {
                empty = false;
            }


            if (empty == false)
            {
                start = new Vector3(gunPos.transform.localPosition.x, gunPos.transform.localPosition.y, gunPos.transform.localPosition.z);
                if (rightHanded == true)
                    end = new Vector3(gunPos.transform.localPosition.x - endDistance, gunPos.transform.localPosition.y, gunPos.transform.localPosition.z);
                else
                    end = new Vector3(gunPos.transform.localPosition.x + endDistance, gunPos.transform.localPosition.y, gunPos.transform.localPosition.z);
                //gunPos.transform.localPosition = transform.TransformPoint(end);
            }
        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetButton("Fire2"))
            {
                SendMessage("HipFire", true);
                hiptimer = 0.0f;
                if (empty == false)
                {
                    float speed = Mathf.Round(ironsighttimer / duration * 10.0f) / 10.0f;
                    if (ironsighttimer < 1.0f)
                    {
                        ironsighttimer += Time.deltaTime;
                        currentPos = Vector3.Lerp(end, start, Curve.Evaluate(speed));
                    }
                }
                if (HeadBobEnabled == true)
                {
                    HeadBob();
                }
                else
                {
                    gunPos.transform.localPosition = currentPos;
                }
                if (shakeActive ==true)
                {
                    DoShake();
                }

            }
            else
            {
                SendMessage("HipFire", false);
                ironsighttimer = 0.0f;
                if (empty == false)
                {
                    float speed = Mathf.Round(hiptimer / duration * 10.0f) / 10.0f;
                    if (hiptimer < 1.0f)
                    {
                        hiptimer += Time.deltaTime;
                        currentPos = Vector3.Lerp(start, end, Curve.Evaluate(speed));
                    }
                }
                if (HeadBobEnabled == true)
                {
                    HeadBob();
                }
                else
                {
                    gunPos.transform.localPosition = currentPos;
                }
                if (shakeActive == true)
                {
                    DoShake();
                }
            }
        }

        void HeadBob()
        {
            float StationaryPosition = Mathf.Min(1, Mathf.Abs(Input.GetAxis("Vertical")) + Mathf.Abs(Input.GetAxis("Horizontal")));
            float PositionXCal = currentPos.x + HeadBobDistance.x * Mathf.Sin(Time.time * HeadBobSpeed + Mathf.PI / 2);
            localPos.x = (currentPos.x * (1 - StationaryPosition)) + (PositionXCal * (StationaryPosition));
            float PositionYCal = currentPos.y + HeadBobDistance.y * Mathf.Sin(Time.time * 2 * HeadBobSpeed);
            localPos.y = (currentPos.y * (1 - StationaryPosition)) + (PositionYCal * (StationaryPosition));
            localPos.z = currentPos.z;
            gunPos.transform.localPosition = localPos;
        }

        void DoShake()
        {
            Vector2 originalPos = new Vector2(currentPos.x, currentPos.y);
            Vector2 playerCamPos = playerCamera.position;
            if (shakeTimer < shakeDur)
            {
                //GunCamera Shake
                Vector2 shakePos = originalPos + (Random.insideUnitCircle * shakeOffset);
                float x = Mathf.SmoothDamp(localPos.x, shakePos.x, ref velocity, shakeFreq);
                float y = Mathf.SmoothDamp(localPos.y, shakePos.y, ref velocity, shakeFreq);
                localPos = new Vector3(x, y, currentPos.z);
                gunPos.transform.localPosition = localPos;
                //Player Camera Shake
                Vector2 playerCamshakePos = playerCamPos + (Random.insideUnitCircle * shakeOffset);
                float xPlayerCam = Mathf.SmoothDamp(playerCamera.position.x, playerCamshakePos.x, ref velocity, shakeFreq);
                float yPlayerCam = Mathf.SmoothDamp(playerCamera.position.y, playerCamshakePos.y, ref velocity, shakeFreq);
                playerCamera.position = new Vector3(xPlayerCam, yPlayerCam, playerCamera.position.z);
                shakeTimer += Time.deltaTime;
            }
            else
            {
                gunPos.transform.localPosition = new Vector3(originalPos.x, originalPos.y, currentPos.z);
                playerCamera.position = new Vector3(playerCamPos.x, playerCamPos.y, playerCamera.position.z);
            }
        }

        void ShakeMe(bool Shake)
        {
            if (Shake == true)
                shakeActive = true;
            else
                shakeActive = false;
        }
    }
}