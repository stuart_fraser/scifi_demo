﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DropdownScript : MonoBehaviour {
    Dropdown dropdown;
    Resolution[] resolutions;

    // Use this for initialization
    void Start () {
        resolutions = Screen.resolutions;
        int i = 0;
        dropdown = GetComponent<Dropdown>();
        foreach (Resolution res in resolutions)
        {
            i ++;
            dropdown.options.Add(new Dropdown.OptionData() { text = res.width+" X "+res.height });
            if (res.width == Screen.width && res.height == Screen.height)
            {
                dropdown.value = i;
            }
        }
        //Below resets window so the 1st value is shown as default.
        dropdown.RefreshShownValue();
        dropdown.onValueChanged.AddListener(delegate {dropdownValueChangedHandler(dropdown);});
    }

    private void dropdownValueChangedHandler(Dropdown target)
    {
        int resWidth = resolutions[target.value].width;
        int resHeight = resolutions[target.value].height;
        Screen.SetResolution(resWidth, resHeight, false);
        PlayerPrefs.SetInt("ResWidth", resWidth);
        PlayerPrefs.SetInt("ResHeight", resHeight);
        PlayerPrefs.Save();
    }
}
