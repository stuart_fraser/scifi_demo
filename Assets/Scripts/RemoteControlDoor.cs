﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemoteControlDoor : MonoBehaviour {
    public GameObject RemoteObject;
    public bool OpenOnStart;
    public bool doOnce = false;
    bool activate = false;
    int usage =0;

	// Use this for initialization
	void Start () {
        if (OpenOnStart == true)
        {
            activate = !activate;
            RemoteObject.SendMessage("Override", activate);
        }
	}
	
    void OnDrawGizmos()
    {
        if (RemoteObject!=null)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawLine(gameObject.transform.position, RemoteObject.transform.position);
        }
    }

	void OnTriggerEnter () {
        if (doOnce == false || usage < 1)
        {
            activate = !activate;
            RemoteObject.SendMessage("Override", activate);
            usage++;
        }
    }

    void OnTriggerExit()
    {
        if (doOnce == false || usage < 1)
        {
            activate = !activate;
            RemoteObject.SendMessage("Override", activate);
            usage++;
        }
    }
}
