﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicAI : MonoBehaviour { 
    public float attackDistance = 10.0f;
    public float walkSpeed = 0.1f;
	public float offset = 0.5f;
    public AudioClip attackSound;
    public float startDelay = 10.0f;
    AudioSource enemySounder;
    Transform player;
    private Animator animator;
    CapsuleCollider basicCollider;
    float animLength = 0.0f;

    GameObject bug;
    GameObject playerObj;

    // Use this for initialization
    void Awake () {
        enemySounder = gameObject.AddComponent(typeof(AudioSource)) as AudioSource;
        enemySounder.clip = attackSound;
        animator = GetComponent<Animator>();   
        basicCollider = GetComponent<CapsuleCollider>();
		bug = this.transform.GetChild(0).GetChild(8).GetChild(0).gameObject;
    }
	
	// Update is called once per frame
	void Update () {
        Invoke("MainAI", startDelay);
	}

    void MainAI()
    {
        bool SetupPlayer = false;
        if (SetupPlayer == false)
        {
            playerObj = GameObject.FindWithTag("Player");
            player = playerObj.transform;
            SetupPlayer = true;
        }
        if (animator.GetCurrentAnimatorStateInfo(0).IsName("Attack"))
        {
            animLength = animator.GetCurrentAnimatorClipInfo(0).Length;
            animLength = animLength / 2;
        }
        if (Vector3.Distance(player.position, this.transform.position) < attackDistance)
        {
            Vector3 direction = player.position - this.transform.position;
            direction.y = 0.0f;
            if (direction != Vector3.zero)
                this.transform.rotation = Quaternion.Slerp(this.transform.rotation, Quaternion.LookRotation(direction), 6.0f * Time.deltaTime);
            animator.SetBool("Idle", false);
            if (direction.magnitude < attackDistance && direction.magnitude > 2.0f)
            {
                gameObject.transform.Translate(0.0f, 0.0f, walkSpeed);
                basicCollider.center = new Vector3(0, 1.5f, 0.0f);
                animator.SetBool("Walking", true);
                animator.SetBool("Attacking", false);
            }
            else
            {
                if (enemySounder != null && !enemySounder.isPlaying)
                    enemySounder.Play();
                animator.SetBool("Walking", false);
                animator.SetBool("Attacking", true);
                basicCollider.center = new Vector3(0, bug.transform.localPosition.y + 1.7f, bug.transform.localPosition.z + 1.0f);
            }
        }
        else
        {
            animator.SetBool("Idle", true);
            animator.SetBool("Walking", false);
            animator.SetBool("Attacking", false);
            basicCollider.center = new Vector3(0, 1.5f, 0.0f);
        }
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Player")
        {
            playerObj.SendMessage("Damage", true);
        }
    }

    void OnCollisionExit(Collision other)
    {
        if (other.gameObject.tag == "Player")
        {
            playerObj.SendMessage("Damage", false);
        }
    }
}
