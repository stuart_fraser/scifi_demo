﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApplyForce : MonoBehaviour
{
    public float thrust = 0.3f;
    List<GameObject> goList;
    Rigidbody RB;
    bool kick = false;

    void Start()
    {
        goList = new List<GameObject>();
    }

    void OnTriggerEnter(Collider other)
    {
        bool active = true;
        if (!goList.Contains(other.transform.root.gameObject))
        {
            //add the object to the list
            if (other.transform.root.gameObject.tag == "Explode")
            goList.Add(other.transform.root.gameObject);
        }
        if (active == false)
        {
            foreach (GameObject go in goList)
            {
                RB = go.GetComponent<Rigidbody>();
                RB.isKinematic = true;
            }
        }
    }

    void OnTriggerStay(Collider other)
    {
        if (!goList.Contains(other.transform.root.gameObject))
        {
            //add the object to the list
            if (other.transform.root.gameObject.tag == "Explode")
                goList.Add(other.transform.root.gameObject);
        }
        if (kick == true)
        {
            foreach (GameObject go in goList)
            {
                RB = go.GetComponent<Rigidbody>();
                RB.isKinematic = false;
                RB.AddForce(new Vector3(1,1,0) * thrust, ForceMode.Impulse);
            }
        }
    }

    void PhysicsKick(bool active)
    {
        kick = active;
    }

    void OnTriggerExit(Collider other)
    {
        if (!goList.Contains(other.transform.root.gameObject))
        {
            //add the object to the list
            if (other.transform.root.gameObject.tag == "Explode")
                goList.Add(other.transform.root.gameObject);
        }
            foreach (GameObject go in goList)
            {
                go.SetActive(false);
            }
    }

    void Update()
    {

    }
}
