﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawning : MonoBehaviour {
    public GameObject SpawnType;
    public GameObject ParticleFX;
    public bool SpawnAtStart=false;
    public float SpawnDelay = 2.0f;
    GameObject mainCamGO;
    Camera GO_Camera;

	// Use this for initialization
	void Start () {
        //Get the main camera in the level.
        mainCamGO = GameObject.FindGameObjectWithTag("MainCamera");
        //Run the spawn function.
        Spawn(SpawnAtStart);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void Spawn(bool active)
    {
        if (active == true)
        {
            //Check to see how far from/angle of the ground is so we can spawn the visual telporter effect.
            Vector3 rayOrigin = gameObject.transform.position;
            RaycastHit hit;
            //Spawn the teleporter effect on the ground.
            if (ParticleFX != null && Physics.Raycast(rayOrigin, Vector3.down, out hit))
            {
                Instantiate(ParticleFX, hit.point, Quaternion.FromToRotation(ParticleFX.transform.position, hit.normal));
            }
            //Spawn the game object (Player,Enemy etc.)
            if (SpawnType.tag == "Player")
            {
                Instantiate(SpawnType, gameObject.transform);
                //Remove from being a child of this spawner object.
                transform.DetachChildren();
                GO_Camera = SpawnType.GetComponentInChildren<Camera>();
                if (GO_Camera != null)
                    mainCamGO.SetActive(false);
            }
            else
                Invoke("DelaySpawn", SpawnDelay);
            //Check to see if the spawned object has a player camera and disable the main camera.
        }
    }

    void DelaySpawn()
    {
        Instantiate(SpawnType, gameObject.transform);
        //Remove from being a child of this spawner object.
        transform.DetachChildren();
    }
}
