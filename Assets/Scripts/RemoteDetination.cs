﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemoteDetination : MonoBehaviour {
    public GameObject ExplodeObject;
    public AudioClip ExplosionNoise;
    public float endTime = 1.0f;
    public bool doOnce = true;
    int usage = 0;
    AudioSource AudioPlayer;

    // Use this for initialization
    void Start () {
        if (ExplodeObject != null)
        {
            ExplodeObject.transform.GetChild(0).gameObject.SetActive(false);
            AudioPlayer = ExplodeObject.AddComponent(typeof(AudioSource)) as AudioSource;
        }
        if (ExplosionNoise != null)
            AudioPlayer.clip = ExplosionNoise;
    }
	
    void OnDrawGizmos()
    {
        if (ExplodeObject != null)
        {
            Gizmos.color = Color.green;
            Gizmos.DrawLine(gameObject.transform.position, ExplodeObject.transform.position);
        }
    }

	void OnTriggerEnter () {
        if (doOnce == false || usage < 1)
        {
            ExplodeObject.transform.GetChild(0).gameObject.SetActive(true);
            if (AudioPlayer != null)
                AudioPlayer.Play();
            ExplodeObject.SendMessage("PhysicsKick", true);
            Invoke("StopFX", endTime);
            usage++;
        }
    }

    void StopFX()
    {
        ExplodeObject.transform.GetChild(0).gameObject.SetActive(false);
    }

}
