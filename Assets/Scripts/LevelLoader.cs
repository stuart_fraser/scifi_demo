﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelLoader : MonoBehaviour {
    public float delayLoad = 3.0f;
    public string SceneToLoad;
    private Animator anim;
    void Start()
    {
        GameObject fade = GameObject.Find("Fade");
        if (fade != null)
        {
            anim = fade.GetComponent<Animator>();
            anim.enabled = false;
        }
    }

    void Update()
    {
        Invoke("LoadLevel", delayLoad);
    }

    void LoadLevel()
    {
        anim.enabled = true;
        if (SceneToLoad != null && anim.GetCurrentAnimatorStateInfo(0).IsName("End"))
            SceneManager.LoadScene(SceneToLoad, LoadSceneMode.Single);
    }
}
