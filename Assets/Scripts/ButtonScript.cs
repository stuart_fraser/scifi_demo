﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public enum MenuOption
{
    Start, Options, Exit, AudioOptions, VideoOptions, Back
}

public class ButtonScript : MonoBehaviour
{
    public GameObject goBackTo;
    public MenuOption currentBtn;
    GameObject MMangerGO;
    string[] optionInfo = new string[3];

    // Use this for initialization
    void Start()
    {
        Button btn = GetComponent<Button>();
        MMangerGO = GameObject.Find("MenuManager");
        btn.onClick.AddListener(TaskOnClick);
    }

    // Update is called once per frame
    void TaskOnClick()
    {
        optionInfo[0] = currentBtn.ToString();
        optionInfo[1] = transform.parent.name.ToString();
        if(goBackTo!=null)
        optionInfo[2] = goBackTo.ToString();
        MMangerGO.SendMessage("ButtonActive", optionInfo);
    }

}
