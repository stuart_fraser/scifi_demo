﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorScript : MonoBehaviour {
    public bool Locked = false;
    public Transform DoorLeft;
    public Transform DoorRight;
    public AnimationCurve Curve = new AnimationCurve(new Keyframe(0.0f, 0.0f), new Keyframe(1.0f, 1.0f));
    public float duration = 2.0f;
    public float endDistance = 2.4f;
    bool empty=true;
    bool open = false;
    float timer = 1.0f;
    Vector3 targetL;
    Vector3 targetR;
    Vector3 startL;
    Vector3 startR;
    AudioSource DoorSound;

    // Use this for initialization
    void Start () {
        if (DoorLeft != null && DoorRight != null)
        {
            empty = false;
            startL = new Vector3(DoorLeft.transform.position.x, DoorLeft.transform.position.y, DoorLeft.transform.position.z);
            startR = new Vector3(DoorRight.transform.position.x, DoorRight.transform.position.y, DoorRight.transform.position.z);
            targetL = new Vector3(DoorLeft.transform.localPosition.x, DoorLeft.transform.localPosition.y, DoorLeft.transform.localPosition.z - endDistance);
            targetR = new Vector3(DoorRight.transform.localPosition.x, DoorRight.transform.localPosition.y, DoorRight.transform.localPosition.z + endDistance);
            targetL = transform.TransformPoint(targetL);
            targetR = transform.TransformPoint(targetR);
        }
        DoorSound = GetComponent<AudioSource>();

    }
	
	// Update is called once per frame
	void Update () {
            if (empty == false && open == true)
            {
                if (timer < 1.0f)
                {
                    timer += Time.deltaTime;
                }
                float speed = Mathf.Round(timer / duration * 10.0f) / 10.0f;
                DoorLeft.transform.position = Vector3.Lerp(DoorLeft.transform.position, targetL, Curve.Evaluate(speed));
                DoorRight.transform.position = Vector3.Lerp(DoorRight.transform.position, targetR, Curve.Evaluate(speed));
            }
            if (empty == false && open == false)
            {
                if (timer < 1.0f)
                {
                    timer += Time.deltaTime;
                }
                float speed = Mathf.Round(timer / duration * 10.0f) / 10.0f;
                DoorLeft.transform.position = Vector3.Lerp(DoorLeft.transform.position, startL, Curve.Evaluate(speed));
                DoorRight.transform.position = Vector3.Lerp(DoorRight.transform.position, startR, Curve.Evaluate(speed));
            }
    }

    void OnTriggerEnter(Collider other)
    {
            if (Locked == false&&(other.tag=="Player"|| other.tag == "Enemy"))
            {
                open = true;
                timer = 0.0f;
                if (DoorSound != null)
                    DoorSound.Play();
            }
    }

    void OnTriggerExit(Collider other)
    {
        if (Locked == false&&(other.tag == "Player" || other.tag == "Enemy"))
        {
            open = false;
            timer = 0.0f;
            if (DoorSound != null)
              DoorSound.Play();
        }
    }

    void Override(bool message)
    {
        if (message == true)
        {
            open = true;
        }
        else
        {
            open = false;
        }
    }
}
