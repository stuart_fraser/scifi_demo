﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AdvancedAI : MonoBehaviour { 
    public float attackDistance = 10.0f;
    public float walkSpeed = 8.0f;
	public float offset = 0.5f;
    public AudioClip attackSound;
    public float startDelay = 10.0f;
    AudioSource enemySounder;
    Transform player;
    private Animator animator;
    CapsuleCollider basicCollider;
    float animLength = 0.0f;

    GameObject bug;
    GameObject playerObj;
    private NavMeshAgent theAgent;
    bool SetupPlayer = false;

    // Use this for initialization
    void Awake () {
        enemySounder = gameObject.AddComponent(typeof(AudioSource)) as AudioSource;
        enemySounder.clip = attackSound;
        animator = GetComponent<Animator>();   
        basicCollider = GetComponent<CapsuleCollider>();
		bug = this.transform.GetChild(0).GetChild(8).GetChild(0).gameObject;
        theAgent = GetComponent<NavMeshAgent>();
    }
	
	// Update is called once per frame
	void Update () {
        Invoke("MainAI", startDelay);
	}

    void MainAI()
    {
        if (SetupPlayer == false)
        {
            playerObj = GameObject.FindWithTag("Player");
            player = playerObj.transform;
            SetupPlayer = true;
        }
        if (animator.GetCurrentAnimatorStateInfo(0).IsName("Attack"))
        {
            animLength = animator.GetCurrentAnimatorClipInfo(0).Length;
            animLength = animLength / 2;
        }
        if (Vector3.Distance(player.position, this.transform.position) < attackDistance)
        {
            Vector3 direction = player.position - this.transform.position;
            direction.y = 0.0f;
            animator.SetBool("Idle", false);
            if (direction.magnitude < attackDistance && direction.magnitude > 2.0f && theAgent.isActiveAndEnabled)
            {
                //gameObject.transform.Translate(0.0f, 0.0f, walkSpeed);
                theAgent.SetDestination(player.position);
                theAgent.speed = walkSpeed;
                basicCollider.center = new Vector3(0, 1.5f, 0.0f);
                animator.SetBool("Walking", true);
                animator.SetBool("Attacking", false);
            }
            else
            {
                if (enemySounder != null && !enemySounder.isPlaying)
                    enemySounder.Play();
                animator.SetBool("Walking", false);
                animator.SetBool("Attacking", true);
                basicCollider.center = new Vector3(0, bug.transform.localPosition.y + 1.7f, bug.transform.localPosition.z + 1.0f);
            }
        }
        else
        {
            animator.SetBool("Idle", true);
            animator.SetBool("Walking", false);
            animator.SetBool("Attacking", false);
            basicCollider.center = new Vector3(0, 1.5f, 0.0f);
        }
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Player"&&SetupPlayer==true)
        {
            playerObj.SendMessage("Damage", true);
        }
    }

    void OnCollisionExit(Collision other)
    {
        if (other.gameObject.tag == "Player" && SetupPlayer == true)
        {
            playerObj.SendMessage("Damage", false);
        }
    }
}
