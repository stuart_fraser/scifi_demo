﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VolumeSettings : MonoBehaviour {
    float newVolume;

    // Use this for initialization
    void Awake ()
    {
        newVolume = PlayerPrefs.GetFloat("Volume");
        AdjustVolume();
    }


    public void AdjustVolume()
    {
        AudioListener.volume = newVolume;
    }
}
