﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour {
    public AudioClip HurtSound;
    public HealthUI healUIScript;
    public float hitDelay = 2.0f;
    public RuntimeAnimatorController fadeOutEffect;
    private Animator AnimateFX;
    private GameObject CanvasObj;

    private float currentSaturation;
    private AudioSource painSound;
    private Image image;
    private bool empty = true;
    private float timer;
    private bool playerHurt = false;
    private Animator fadeOut;
    private RuntimeAnimatorController fadeInEffect;
    private Vector3 localSpawnPos;
    private Quaternion localSpawnRot;



    void Start()
    {
        localSpawnPos = gameObject.transform.position;
        localSpawnRot = gameObject.transform.rotation;
        CanvasObj = GameObject.Find("HurtCanvas");
        if (CanvasObj != null)
            empty = false;
        if (empty == false)
        {
            AnimateFX = CanvasObj.GetComponentInChildren<Animator>();
            AnimateFX.SetBool("HurtFX", false);
            image = CanvasObj.GetComponentInChildren<Image>();
            image.enabled = false;
            fadeOut = CanvasObj.transform.GetChild(1).gameObject.GetComponent<Animator>();
            fadeInEffect = fadeOut.runtimeAnimatorController;
        }
        painSound = gameObject.AddComponent(typeof(AudioSource)) as AudioSource;
        painSound.clip = HurtSound;
        HealthUI.health = 100;
        timer = hitDelay;
    }


    // Update is called once per frame
    void Damage (bool hit) {
        if (empty == false)
        {
            if (hit == true)
            {
                SetHealth();
            }
            else
                decreaseAlpha();
        }
    }

    void increaseAlpha()
    {
        image.enabled = true;
        AnimateFX.SetBool("HurtFX", true);
        if (painSound != null && !painSound.isPlaying)
            painSound.Play();
    }

    void decreaseAlpha()
    {
        AnimateFX.SetBool("HurtFX", false);
    }

    void SetHealth()
    {
        playerHurt = true;
    }

    void Update()
    {
        if (HealthUI.health > 0 && timer == hitDelay && playerHurt == true)
        {
            HealthUI.health -= 10;
            increaseAlpha();
            timer -= Time.deltaTime;
        }
        //Respawn on health being zero (may need to change this so happens between low health and death)
        if (HealthUI.health <= 0 && timer == hitDelay && playerHurt == true)
        {
            if (fadeOut != null)
            {
                if (fadeOut.GetCurrentAnimatorStateInfo(0).IsName("End"))
                {
                    HealthUI.health = 100;
                    gameObject.transform.position = localSpawnPos;
                    gameObject.transform.rotation = localSpawnRot;
                    fadeOut.runtimeAnimatorController = fadeInEffect;
                }
                else
                    fadeOut.runtimeAnimatorController = fadeOutEffect;
            }
        }
        if (timer>=0.0f && timer<hitDelay)
        {
            timer -= Time.deltaTime;
        }
        else
        {
            timer = hitDelay;
        }
        playerHurt = false;
    }
}
