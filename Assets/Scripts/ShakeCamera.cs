﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShakeCamera : MonoBehaviour {
    public float shakeDur = 0.5f;
    public float shakeOffset = 10.0f;
    public float shakeFrequency = 0.1f;
    public bool shakeMe;
    GameObject player;
    Vector2 originalPos;
    float shakeTimer;

	// Use this for initialization
	void OnTriggerEnter(Collider other)
    {
        if (other.tag=="Player")
        {
            player= other.gameObject;
            player.SendMessage("ShakeMe", true);            
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (player != null)
        {
            player.SendMessage("ShakeMe", false);
        }
    }

}
