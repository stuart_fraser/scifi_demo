﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleScript : MonoBehaviour {
    public Toggle currentToggle;
    int newToggle;

	// Use this for initialization
	void Start () {
        currentToggle = GetComponent<Toggle>();
        newToggle = PlayerPrefs.GetInt("Fullscreen");
        if (newToggle==0)
        {
            currentToggle.isOn = false;
        }
        else
        {
            currentToggle.isOn = true;
        }
        currentToggle.onValueChanged.AddListener(delegate { ChangedToggle(currentToggle); });
    }

    public void ChangedToggle(Toggle update)
    {

        Screen.SetResolution(Screen.width, Screen.height, update.isOn);
        if (update.isOn == false)
        {
            newToggle = 0;
        }
        else
        {
            newToggle = 1;
        }
        PlayerPrefs.SetInt("Fullscreen", newToggle);
        PlayerPrefs.Save();
    }
}
