﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthUI : MonoBehaviour {
    Text HealthText;
    [HideInInspector]
    public static int health;

	// Use this for initialization
	void Start () {
        HealthText = GetComponentInChildren<Text>();
	}
	
	// Update is called once per frame
	void Update () {
        HealthText.text = health.ToString();
    }
}
