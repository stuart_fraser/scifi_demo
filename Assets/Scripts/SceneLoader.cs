﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour {

	// Use this for initialization
	void Start () {
        SceneManager.LoadScene("CanvasFX", LoadSceneMode.Additive);
    }
}
