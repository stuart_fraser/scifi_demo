﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour {
    public AudioClip buttonClick;
    AudioSource btnNoise;
    string remoteActive;
    string remoteCurrent;
    string remoteBack;
    [Tooltip("Include all selectable menus in this list, by default the first entry is own enabled at start.")]
    public GameObject[] SelectableMenus;
    int resWidth=0;
    int resHeight=0;
    int fullScreen=0;
    bool newToggle;

    // Use this for initialization
    void Start () {
        //Set the saved resolution.
        resWidth=PlayerPrefs.GetInt("ResWidth");
        resHeight=PlayerPrefs.GetInt("ResHeight");
        fullScreen = PlayerPrefs.GetInt("Fullscreen");
        if (fullScreen == 0)
        {
            newToggle = false;
        }
        else
        {
            newToggle = true;
        }
        if (resWidth!=0&&resHeight!=0)
        Screen.SetResolution(resWidth, resHeight, newToggle);

        //Set button sound effect to audiosource.
        btnNoise = gameObject.AddComponent(typeof(AudioSource)) as AudioSource;
        for (int i=0;i<SelectableMenus.Length;i++)
        {
            if (i==0)
            {
                SelectableMenus[i].SetActive(true);
            }
            else
            {
                SelectableMenus[i].SetActive(false);
            }
        }
    }

    void ButtonActive(string[] optionInfo)
    {
        remoteActive = optionInfo[0];
        remoteCurrent = optionInfo[1];
        remoteBack = optionInfo[2];

        if (buttonClick != null)
        {
            btnNoise.clip = buttonClick;
            btnNoise.Play();
            Invoke("OptionSelect", btnNoise.clip.length);
        }
    }

void OptionSelect()
    {
        switch (remoteActive)
        {
            case "Start":
                SceneManager.LoadScene("LoadingScreen", LoadSceneMode.Single);
                break;
            case "Options":
                SelectableMenus[0].SetActive(false);
                SelectableMenus[1].SetActive(true);
                break;
            case "Exit":
                Application.Quit();
                break;
            case "AudioOptions":
                SelectableMenus[1].SetActive(false);
                SelectableMenus[2].SetActive(true);
                break;
            case "VideoOptions":
                SelectableMenus[1].SetActive(false);
                SelectableMenus[3].SetActive(true);
                break;
            case "Back":
                if (remoteBack!=null)
                {
                    for (int i = 0; i < SelectableMenus.Length; i++)
                    {
                        string compare = SelectableMenus[i].ToString();
                        if (String.Equals(compare, remoteBack) == true)
                        {
                            SelectableMenus[i].SetActive(true);
                        }
                    }
                        GameObject.Find(remoteCurrent).SetActive(false);
                }
                break;
        }
    }
}
