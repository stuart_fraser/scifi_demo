﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderScript : MonoBehaviour {
    public Slider currentSlider;
    GameObject activeListener;
    float newVolume;

    // Use this for initialization
    public void Start()
    {
        activeListener = Camera.main.gameObject;
        newVolume = PlayerPrefs.GetFloat("Volume");
        currentSlider.value = newVolume;
        currentSlider.onValueChanged.AddListener(delegate { ChangedSlider(); });
	}

    public void ChangedSlider()
    {
        newVolume =  currentSlider.value;
        activeListener.SendMessage("AdjustVolume", newVolume);
        PlayerPrefs.SetFloat("Volume", newVolume);
        PlayerPrefs.Save();
    }
}
