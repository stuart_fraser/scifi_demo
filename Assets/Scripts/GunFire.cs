﻿using System.Collections;
using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace UnityStandardAssets.Characters.FirstPerson
{
    public class GunFire : MonoBehaviour
    {
        public float fireRate = 0.25f;
        public float weaponRange = 20.0f;
        public float hitMaxVolume = 0.5f;
        public float weaponDamage = 25.0f;
        public float kickStrength = 10.0f;


        public Camera gunCamera;
        public Transform spawnPoint;
        public ParticleSystem muzzleFlash;
        public ParticleSystem cartridgeExpel;
        public Effects ParticleEffects;
        public Sounds SoundEffects;
        public AudioClip shotEffect;
        public AudioClip shellEffect;

        public bool DebugRay = false;

        [Serializable]
        public class Effects
        {
            public GameObject metalHitEffect;
            public GameObject alienHitEffect;
            public GameObject[] fleshHitEffects;
        }

        [Serializable]
        public class Sounds
        {
            public AudioClip metalSoundEffect;
            public AudioClip alienSoundEffect;
            public AudioClip fleshSoundEffect;
        }

        float nextFire = 0.0f;
        bool triggerPress = false;
        AudioSource gunSound;
        Vector3 rayOrigin;

        // Use this for initialization
        void Start()
        {
            gunSound = gameObject.AddComponent(typeof(AudioSource)) as AudioSource;
            gunSound.clip = shotEffect;
        }

        // Update is called once per frame
        void Update()
        {
			Vector3 cameraRot = Camera.main.transform.rotation.eulerAngles;
            spawnPoint.rotation = Quaternion.Euler(cameraRot.x - 0.0f, cameraRot.y,cameraRot.z);
            if (DebugRay == true)
            Debug.DrawRay(rayOrigin, spawnPoint.forward*10.0f, Color.red);
            if (triggerPress == true)
                nextFire += Time.deltaTime;
            if (nextFire > fireRate)
            {
                nextFire = 0;
                triggerPress = false;
            }
            if (Input.GetButton("Fire1"))
            {
                triggerPress = true;
                gunSound.pitch = (Random.Range(0.8f, 1.1f));
                if (shotEffect != null && !gunSound.isPlaying)
                    gunSound.Play();
                if (nextFire == 0.0f)
                    triggerPulled();
            }
        }

        void triggerPulled()
        {
            int layerMask = -1;
            muzzleFlash.Play();
            cartridgeExpel.Play();
            AudioSource expelSound = cartridgeExpel.gameObject.AddComponent<AudioSource>();
            expelSound.clip = shellEffect;
            if (shellEffect != null && !expelSound.isPlaying)
                expelSound.Play();
                RaycastHit hit;
            if (Physics.Raycast(rayOrigin, spawnPoint.forward, out hit, weaponRange, layerMask, QueryTriggerInteraction.Ignore))
                {
                if (hit.collider.CompareTag("Enemy"))
                {
                    GameObject hitName = hit.collider.gameObject;
                    GameObject hitParent = hitName.transform.root.gameObject;
                    hitParent.SendMessage("Hit", weaponDamage);
                }
                if (hit.rigidbody != null && !hit.collider.CompareTag("Enemy"))
                {
                    hit.rigidbody.AddForceAtPosition(Camera.main.transform.forward * kickStrength, hit.point, ForceMode.Impulse);
                }
                matHit(hit);
                }
        }

        void matHit(RaycastHit hit)
        {
            if (hit.collider.sharedMaterial != null)
            {
                string materialName = hit.collider.sharedMaterial.name;

                switch (materialName)
                {
                    case "Metal":
                        spawnDecal(hit, ParticleEffects.metalHitEffect, SoundEffects.metalSoundEffect);
                        break;
                    case "Alien":
                        spawnDecal(hit, ParticleEffects.alienHitEffect, SoundEffects.alienSoundEffect);
                        break;
                    case "Character":
                        spawnDecal(hit, ParticleEffects.fleshHitEffects[Random.Range(0, ParticleEffects.fleshHitEffects.Length)], SoundEffects.fleshSoundEffect);
                        break;
                }
            }
        }

        void spawnDecal(RaycastHit hit, GameObject prefab, AudioClip soundFX)
        {
            GameObject spawnedDecal = GameObject.Instantiate(prefab, hit.point, Quaternion.LookRotation(hit.normal));
            AudioSource hitSound = spawnedDecal.AddComponent<AudioSource>();
            hitSound.volume = hitMaxVolume;
            hitSound.clip = soundFX;
            hitSound.pitch = (Random.Range(1.3f, 1.5f));
            hitSound.maxDistance = 4.0f;
            hitSound.spatialBlend = 1.0f;
            hitSound.rolloffMode = AudioRolloffMode.Linear;
            hitSound.Play();
            spawnedDecal.transform.SetParent(hit.collider.transform);
        }

		void HipFire (bool ironsight)
		{
			if (ironsight == false)
			{
				rayOrigin = spawnPoint.position;
			}
			 else
			{
				if (gunCamera!=null)
				{
					Ray ray = gunCamera.ViewportPointToRay(new Vector3(0.5f,0.5f,0.0f));
					rayOrigin = ray.origin;
				}
				else
				Debug.LogError("Attach GunCamera!");
			}
		}
    }
}
