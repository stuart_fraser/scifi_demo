﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class FadeIntensity : MonoBehaviour {
    public float duration = 10.0f;
    public float minIntensity = 0.0f;
    public float maxIntensity = 1.0f;
    public bool looping = true;
    Light _light;
    bool fadeUp=false;
    float currentTime;
    float timer = 1.0f;
    int fadeCount;

    void Start()
    {
        _light = GetComponent<Light>();
    }

	// Update is called once per frame
	void Update () {
        timer = Time.deltaTime;
        if (fadeUp==true)
        {
            currentTime += timer/duration;
            fadeCount++;
        }
        if (fadeUp == false)
        {
            currentTime -= timer/duration;
            fadeCount++;
        }
        if (_light.intensity >= maxIntensity)
        {
            fadeUp = false;
        }
        if (_light.intensity <= minIntensity)
        {
            fadeUp = true;
        }
        if (fadeCount < 2 || looping == true)
        {
            float tempIntensity = Mathf.Lerp(minIntensity, maxIntensity, currentTime);
            _light.intensity = Mathf.Round(tempIntensity * 10.0f) / 10.0f;
        }
    }
}
