﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleEnabled : MonoBehaviour
{
    public ParticleSystem _Particle;
    public float Delay = 3.0f;
    private float Timer;
    private bool Active = false;

    // Update is called once per frame
    void Update()
    {
        Timer += Time.deltaTime;
        if (Timer >= Delay)
        {
            Timer = 0.0f;
            Active = !Active;
            if (Active == true)
            {
                _Particle.Stop();
            }
            else
            {
                _Particle.Play();
            }
        }
    }
}
