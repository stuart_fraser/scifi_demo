﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerSpawner : MonoBehaviour {
    public GameObject[] SpawnerToActive;
    public int TriggerAmount = 1;
    private int Counter=0;
	// Use this for initialization
	void OnTriggerEnter (Collider other) {
        if (Counter < TriggerAmount)
        {
            if (SpawnerToActive != null && other.tag == "Player")
            {
                foreach (GameObject Spawner in SpawnerToActive)
                {
                    Spawner.SendMessage("Spawn", true);
                }
                Counter++;
            }
        }
    }

    void OnDrawGizmos()
    {
        if (SpawnerToActive != null)
        {
            foreach (GameObject Spawner in SpawnerToActive)
            {
                Gizmos.color = Color.blue;
                Gizmos.DrawLine(gameObject.transform.position, Spawner.transform.position);
            }
        }

    }
}
