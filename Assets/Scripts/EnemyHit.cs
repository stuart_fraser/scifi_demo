﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyHit : MonoBehaviour
{
    public float health = 100.0f;
    public AudioClip hurtSound;
    public float fadePerSecond = 1.0f;
    public float InvincibilityDelay = 3.0f;
    RaycastHit hit;
    AudioSource enemySounder;
    private Animator animator;
    private NavMeshAgent theAgent;
    BasicAI AIScript;
    CapsuleCollider CC;
    GameObject Mesh;
    int LayerID;
    Rigidbody RB;
    Transform CCTransform;
    float Timer = 0.0f;

    void Start()
    {
        enemySounder = gameObject.AddComponent(typeof(AudioSource)) as AudioSource;
        enemySounder.clip = hurtSound;
        animator = GetComponent<Animator>();
        theAgent = GetComponent<NavMeshAgent>();
        AIScript = GetComponent<BasicAI>();
        CC = GetComponent<CapsuleCollider>();
        //CCTransform = CC.transform;
        RB = GetComponent<Rigidbody>();
        LayerID = LayerMask.NameToLayer("IgnorePlayer");
        Mesh = this.transform.GetChild(1).gameObject;
    }

    // Use this for initialization
    void Hit(float hit)
    {
        if (Timer > InvincibilityDelay)
        {
            health -= hit;
            if (enemySounder != null && !enemySounder.isPlaying)
                enemySounder.Play();
            if (health <= 0.0f)
            {
                animator.SetBool("Dead", true);
                //CC.transform.position = CCTransform.position;
                theAgent.enabled = false;
                //This is using the older script we can safely remove if not using anymore.
                if(AIScript!=null)
                AIScript.enabled = false;
                //END
                gameObject.layer = LayerID;
                RemoveColliders();
                RB.isKinematic = true;
                CC.isTrigger = false;
                enemySounder = null;
                RB.isKinematic = false;
            }
        }
    }

    void Update()
    {
        Timer += Time.deltaTime;
        if (animator.GetBool("Dead") == true && animator.GetCurrentAnimatorStateInfo(0).normalizedTime > 1 && animator.GetCurrentAnimatorStateInfo(0).IsName("Dead"))
        {
            SkinnedMeshRenderer Mat = Mesh.GetComponent<SkinnedMeshRenderer>();
            Color color = Mat.material.color;
            float fadeTime = Mathf.Clamp(color.a - fadePerSecond * Time.deltaTime, 0.0f, 1.0f);
            //All of this to change the standard opaque material to cutout...
            Mat.material.SetFloat("_Mode", 2);
            Mat.material.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
            Mat.material.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
            Mat.material.SetInt("_ZWrite", 0);
            Mat.material.DisableKeyword("_ALPHATEST_ON");
            Mat.material.EnableKeyword("_ALPHABLEND_ON");
            Mat.material.DisableKeyword("_ALPHAPREMULTIPLY_ON");
            Mat.material.renderQueue = 3000;
            //Now blend the alpha.
            Mat.material.color = new Color(color.r, color.g, color.b, fadeTime);
            if (fadeTime == 0.0f)
            {
                Destroy(gameObject);
            }
        }
    }


    private void RemoveColliders()
    {
        Collider[] childColliders;
        int id=0;
        childColliders = gameObject.GetComponentsInChildren<Collider>();
        foreach (Collider collider in childColliders)
        {
            if (id!=0)
            Destroy(collider);
            id++;
        }
    }
}
